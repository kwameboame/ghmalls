from django.db import models

from django.dispatch import receiver
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from taggit.managers import TaggableManager
from avatar.models import Avatar


class Mall(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	name = models.CharField(max_length=255)
	display_pic = models.ImageField(upload_to='mall_dp', null=True)
	address = models.CharField(max_length=255)

	def __str__(self):
		return self.name
		
class Store(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	name = models.CharField(max_length=255)
	logo = models.ImageField(upload_to="store_logos", null=True)
	opening_time = models.CharField(max_length=255, null=True, blank=True)
	closing_time = models.CharField(max_length=255, null=True, blank=True)
	description = models.TextField()
	mall = models.ManyToManyField(Mall)
	store_tags = TaggableManager()

	def __str__(self):
		return self.name

class Item(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	name = models.CharField(max_length=255)
	price = models.CharField(max_length=10)
	onsale = models.BooleanField(default=False)
	images = models.ImageField(upload_to="item_images", null=True)
	sale_price = models.CharField(max_length=10, null=True, blank=True)
	description = models.TextField()
	mall = models.ManyToManyField(Mall)
	store = models.ManyToManyField(Store)
	item_tags = TaggableManager()

	def __str__(self):
		return self.name


class Profile(models.Model):
	GENDER_CHOICES = (
		('M', 'Male'),
		('F', 'Female'),
	)
	created_at = models.DateTimeField(auto_now_add=True)
	user = models.OneToOneField('auth.user')
	first_name = models.CharField(max_length=255, null=True)
	last_name = models.CharField(max_length=255, null=True)
	gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
	cell_number = models.CharField(max_length=10, default='Mobile #', unique=True)
	bio = models.TextField(max_length=200, null=True)
	date_of_birth = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)

	def __str__(self):
		return str(self.user)
		
@receiver(post_save, sender=User)
def user_profile(sender, **kwargs):
	user = kwargs.get('instance')
	created = kwargs.get('created')
	if created:
		Profile.objects.create(user=user)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
	if created:
		Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
	instance.profile.save()


class Mall_Visit(models.Model):
	visit_date_time = models.DateTimeField(auto_now_add=True)
	mall = models.ForeignKey(Mall)
	user = models.ForeignKey(User)


class Store_Visit(models.Model):
	visit_date_time = models.DateTimeField(auto_now_add=True)
	store = models.ForeignKey(Store)
	user = models.ForeignKey(User)