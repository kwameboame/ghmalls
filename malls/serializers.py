from rest_framework import serializers
from .models import Mall, Store, Item, Profile, Mall_Visit, Store_Visit
from avatar.models import Avatar
from django.contrib.auth.models import User
from taggit.models import TaggedItem


class MallSerializer(serializers.ModelSerializer):
	class Meta:
		model = Mall
		fields = '__all__'


class StoreSerializer(serializers.ModelSerializer):
	class Meta:
		model = Store
		fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):
	class Meta:
		model = Item
		fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = '__all__'

class AvatarSerializer(serializers.ModelSerializer):
	class Meta:
		model = Avatar
		fields = '__all__'
		

class Mall_VisitSerializer(serializers.ModelSerializer):
	class Meta:
		model = Mall_Visit
		fields = '__all__'

class Store_VisitSerializer(serializers.ModelSerializer):
	class Meta:
		model = Store_Visit
		fields = '__all__'


class TagSerializer(serializers.ModelSerializer):

	class Meta:
		model = TaggedItem
		fields = '__all__'


class UserSerializer(serializers.ModelSerializer):

	password = serializers.CharField(
		write_only=True,
	)

	class Meta:
		model = User
		fields = ('password', 'username', 'first_name', 'last_name',)

	def create(self, validated_data):
		user = super(UserSerializer, self).create(validated_data)
		if 'password' in validated_data:
			user.set_password(validated_data['password'])
			user.save()
		return user