from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.views.generic import DetailView, ListView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from malls.models import Profile, Store, Mall, Item, Mall_Visit, Store_Visit
from avatar.models import Avatar
from taggit.models import TaggedItem
from malls.serializers import MallSerializer, StoreSerializer, TagSerializer, ItemSerializer, ProfileSerializer, AvatarSerializer, UserSerializer, Mall_VisitSerializer, Store_VisitSerializer


class TagMixin(object):
	def get_context_data(self, **kwargs):
		context = super(TagMixin, self).get_context_data(**kwargs)
		context['tags'] = Tag.objects.all()
		return context
	

class ProfileDetailView(DetailView):
	template_name = 'profile.html'
	model = Profile
	context_object_name = 'profile'


class TagIndexView(TagMixin, ListView):
	template_name = 'item_tag_index.html'
	model = Item
	paginate_by = '10'
	context_object_name = 'item_tags'

	def get_queryset(self):
		return Item.objects.filter(tags__slug=self.kwargs.get('slug'))




## API Views
# malls/
class MallApiView(APIView):
	def get(self, request):
		malls = Mall.objects.all()
		serializer = MallSerializer(malls, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = MallSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		

class ProfileApiView(APIView):
	def get(self, request):
		profiles = Profile.objects.all()
		serializer = ProfileSerializer(profiles, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = ProfileSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		

class AvatarApiView(APIView):
	def get(self, request):
		avatars = Avatar.objects.all()
		serializer = AvatarSerializer(avatars, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = AvatarSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		

class ItemApiView(APIView):
	def get(self, request):
		items = Item.objects.all()
		serializer = ItemSerializer(items, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = ItemSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		

class StoreApiView(APIView):
	def get(self, request):
		stores = Store.objects.all()
		serializer = StoreSerializer(stores, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = StoreSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		


class UserApiView(APIView):
	def get(self, request):
		users = User.objects.all()
		serializer = UserSerializer(users, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = UserSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Mall_VisitApiView(APIView):
	def get(self, request):
		mall_visits = Mall_Visit.objects.all()
		serializer = Mall_VisitSerializer(mall_visits, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = Mall_VisitSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Store_VisitApiView(APIView):
	def get(self, request):
		store_visits = Store_Visit.objects.all()
		serializer = Mall_VisitSerializer(store_visits, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = Mall_VisitSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TagApiView(APIView):
	def get(self, request):
		tags = TaggedItem.objects.all()
		serializer = TagSerializer(tags, many=True)
		return Response(serializer.data)

	def post(self, request):
		serializer = TagSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.data, status=status.HTTP_201_CREATED)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


