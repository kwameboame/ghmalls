# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-21 13:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('malls', '0009_auto_20161221_0928'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='date_of_birth',
        ),
        migrations.RemoveField(
            model_name='store',
            name='closing_time',
        ),
        migrations.RemoveField(
            model_name='store',
            name='opening_time',
        ),
    ]
