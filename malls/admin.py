from django.contrib import admin
from .models import Profile, Store, Mall, Item


admin.site.register(Mall)
admin.site.register(Store)
admin.site.register(Item)
admin.site.register(Profile)
