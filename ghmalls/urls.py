"""ghmalls URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from registration import views
from malls.views import TagIndexView, ProfileDetailView
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_swagger.views import get_swagger_view
from malls import views

schema_view = get_swagger_view(title='Ghana Malls API')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tag/(?P<slug>[-\w]+)/$', TagIndexView.as_view(), name='tagged'),
    url(r'(?P<pk>\d+)/$', ProfileDetailView.as_view(), name='profile'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^avatar/', include('avatar.urls')),
    #API URLs/Endpoints
    url(r'^api/$', schema_view),
    url(r'^api/malls/', views.MallApiView.as_view()),
    url(r'^api/stores/', views.StoreApiView.as_view()),
    url(r'^api/items/', views.ItemApiView.as_view()),
    url(r'^api/profiles/', views.ProfileApiView.as_view()),
    url(r'^api/user/', views.UserApiView.as_view()),
    url(r'^api/mall_visits/', views.Mall_VisitApiView.as_view()),
    url(r'^api/store_visits/', views.Store_VisitApiView.as_view()),
    url(r'^api/avatar/', views.AvatarApiView.as_view()),
    url(r'^api/tagged_items/', views.TagApiView.as_view()),
]


urlpatterns = format_suffix_patterns(urlpatterns)

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)